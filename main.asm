%include "lib.inc"
%include "colon.inc"
%include "words.inc"
%include "dict.inc"

%define BUF_SIZE 256
%define SSIZE 8

section .rodata
error_wrong_str: db "Too long input", 10, 0
error_wrong_inp: db "There is wrong key", 10, 0

section .bss
buf: resb BUF_SIZE

section .text
global _start

_start:
        mov rsi, BUF_SIZE
        mov rdi, buf
        call read_word

        test rax, rax
        jz .buf_overflow

        mov rdi, buf
        mov rsi, BIBA
        call find_word

        test rax, rax
        jz .wrong_inp

         mov rdi, rax
        add rdi, SSIZE
        call string_length
        lea rdi, [rdi+rax+1]
        call print_string
        xor rdi, rdi
        jmp .end
.buf_overflow:
        mov rdi, error_wrong_str
        call print_error_string
        mov rdi, 1
        jmp .end
.wrong_inp:
        mov rdi, error_wrong_inp
        call print_error_string
        mov rdi, 1
.end:
        call exit
