%define BIBA 0

%macro colon 2
    %ifstr %1
        %ifid %2
                %2: dq BIBA
                db %1, 0
                %define BIBA %2
        %else
                %error "second is not id"
        %endif
    %else
        %error "first is not string"
    %endif
%endmacro
