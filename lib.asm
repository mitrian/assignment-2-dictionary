global exit
global string_length
global print_string
global print_error_string
global print_newline
global print_char
global print_int
global print_uint
global string_equals
global read_char
global read_word
global parse_int
global parse_uint
global string_copy

section .text

%define SYS_EXIT 60
%define SYS_WRITE 1
%define STDIN 0
%define STDOUT 1
%define STDERR 2
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, SYS_EXIT
    syscall



; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax

    .loop:
        cmp byte [rdi+rax], 0
        je .end
        inc rax
        jmp .loop

    .end:
        ret



; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, SYS_WRITE
    mov rdi, STDOUT
    syscall
    ret

print_error_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, SYS_WRITE
    mov rdi, STDERR
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`



; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi
    mov rax, SYS_WRITE
    mov rdi, STDOUT
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret



; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp  rdi, 0
    jge  print_uint
    neg  rdi
    push rdi
    mov  rdi, '-'
    call print_char
    pop  rdi



; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rbp
    mov rax, rdi
    dec rsp
    mov byte[rsp], 0 ; '0' -> top of the stack
    mov rbp, 1
    mov r8, 10

    .loop:
        xor rdx, rdx
        div r8
        dec rsp
        inc rbp
        add dl, '0'
        mov [rsp], dl
        test rax, rax
        jz .print
        jmp .loop

    .print:
        mov rdi, rsp
        call print_string

    add rsp, rbp
    pop rbp
    ret



; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax

    .loop:
        mov r9b, byte [rdi+rax]
        cmp r9b, byte [rsi+rax]
        jne .end
        cmp r9b , 0
        je .equals
        inc rax
        jmp .loop

    .equals:
        mov rax,1
        ret

    .end:
        xor rax, rax
        ret



; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push rax
    mov rsi, rsp
    mov rdi, STDIN
    mov rdx, 1
    syscall
    pop rax
    ret



; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor rax, rax
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    xor r14, r14
    cmp rsi, 0
    je .end

    .loop:
        call read_char
        test rax, rax
        je .end
        cmp r14, r13
        jnl .over
        cmp ax, ` `
        je .skip
        cmp ax, `\t`
        je .skip
        cmp ax, `\n`
        je .skip
        mov [r12 + r14], ax
        inc r14
        jmp .loop

    .over:
        xor rax, rax
        pop r14
        pop r13
        pop r12
        ret

    .skip:
        test r14, r14
        je .loop

    .end:
        mov byte[r12+r14], 0
        mov rax, r12
        mov rdx, r14
        pop r14
        pop r13
        pop r12
        ret



; Принимает указатель на строку, пытается
; проитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rd = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor rcx, rcx

    .loop:
        mov dl, byte [rdi+rcx]
        sub dl, '0' ;char->numb
        cmp dl, 9
        ja .end
        imul rax,rax, 10
        add rax, rdx
        inc rcx
        jmp .loop

    .end:
        mov rdx, rcx
        ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    xor rcx, rcx
    mov dl, byte [rdi+rcx]
    cmp dl, '+'
    je .pls
    cmp dl, '-'
    je .minus
    jmp parse_uint

    .pls:
        inc rdi
        call parse_uint
        inc rdx
        ret
    .minus:
        inc rdi
        call parse_uint
        inc rdx
        neg rax
        ret



; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor  rax, rax

    .loop:
        mov  cl, [rdi + rax]
        mov  [rsi + rax], cl
        test cl, cl
        jz   .ready
        inc  rax
        cmp  rax, rdx
        jl   .loop
        xor  rax, rax

    .ready:
        ret
