import subprocess
from itertools import groupby

command = "./main"
dict= {"first_word":["word_1",""], "second_word":["word_2",""], "third_word":["word_3",""],
 "BIBABOBA":["","There is wrong key"], "max"*66:["","Too long input"]}

print("\n"+"TESTS ARE RUNNING"+"\n")
print("~"*50)
total_mistakes = []
mistake_numb = 0
for input in dict:
    process = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess>
    stdout, stderr = process.communicate(input.encode())
    mistake_numb+=1
    stdoutt = stdout.decode()
    stderrr = stderr.decode()

    if stdoutt == dict[input][0]:
        print(f"Answer for {input} is right!")
    else:
        total_mistakes.append(str(mistake_numb))
        print(f"Wrong output for {input}: {stdoutt}, expected: {dict[input][0]}")
    if stderrr != dict[input][1]:
        total_mistakes.append(str(mistake_numb))
        print(f"Wrong stderr for {input}: {stderr}, expected: {dict[input][1]}")
    print("~"*50)
total_mistakes= [el for el, _ in groupby(total_mistakes)]
print("\n")
if len(total_mistakes)==0:
    print("All tests are complited")
else:
    print("Something went wrong in tests:")
    outp = ''
    for i in total_mistakes[:-1]:
        outp= outp + i + ", "
    outp =outp + total_mistakes[-1]
    print(outp+"\n")
