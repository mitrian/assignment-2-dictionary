%include "lib.inc"
%define SSIZE 8

section .text

global find_word

; rdi ~ pointer of null-term string
; rsi ~ pointer of begining of dict
find_word:
        push r12
        push r13
        mov r12, rdi
        mov r13, rsi
        .loop:
                test r13, r13 ; move to the end
                jz .not_found
                mov rdi, r12
                lea rsi, [r13+SSIZE] ; move pointer on next begining of el -> equasl with rdi
                call string_equals
                test rax, rax
                jnz .found ; everything is ok
                mov r13, [r13] ; next pair
                jmp .loop
        .not_found:
                xor rax, rax
                jmp .end
        .found:
                mov rax, r13
        .end:
                pop r13
                pop r12
                ret

