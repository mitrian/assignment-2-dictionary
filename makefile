DEBUG=true
FORMAT=elf64

ASM_FILES=$(wildcard ./*.asm)
OBJ_FILES=$(ASM_FILES:%.asm=%.o)

.PHONY: clean run test
clean:
        rm -rf *.o
        rm -rf main

test: clean build
        python3 test.py
        rm -rf main
        rm -rf *.o
        rm -rf main

%.o:    %.asm
        nasm -g -f $(FORMAT) -o $@ $<

main.o: main.asm        $(wildcard ./*.inc)

build: main

main:  $(OBJ_FILES)
        ld -o $@ $^

run: build
        chmod a+x main
        ./main

